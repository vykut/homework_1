function distance(first, second){
	//TODO: implementați funcția
	// TODO: implement the function

	if(Array.isArray(first) && Array.isArray(second)) {
		first = removeDuplicates(first)
		second = removeDuplicates(second)

		return diff(first, second).length
	} else
	throw {message: 'InvalidType'}
}

function removeDuplicates(array) {
	return array.filter((a, b) => array.indexOf(a) === b)
};

function diff(first, second) {
	let diff = []
	for(let i = 0; i<first.length;i++)
		if(!second.includes(first[i]))
			diff.push(first[i])
	for(let j=0;j<second.length;j++)
		if(!first.includes(second[j]))
			diff.push(second[j])
	return diff
};

module.exports.distance = distance